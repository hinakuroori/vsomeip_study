#include <iomanip>
#include <iostream>
#include <thread>
#include <unistd.h>
#include <vsomeip/vsomeip.hpp>

#define SAMPLE_SERVICE_ID 0x1234
#define SAMPLE_INSTANCE_ID 0x5678
#define SAMPLE_EVENTGROUP_ID 0xdead
#define SAMPLE_EVENT_ID 0xbeef

std::shared_ptr<vsomeip::application> app;

static void publish(void)
{
	const vsomeip::byte_t its_data[] = {0x10};
	std::shared_ptr<vsomeip::payload> payload;
	payload = vsomeip::runtime::get()->create_payload();
	payload->set_data(its_data, sizeof(its_data));
	std::set<vsomeip::eventgroup_t> its_groups;
	its_groups.insert(SAMPLE_EVENTGROUP_ID);
	app->offer_event(SAMPLE_SERVICE_ID, SAMPLE_INSTANCE_ID,
		SAMPLE_EVENT_ID, its_groups);
	std::cout << "Invoke notification." << std::endl;
	while (1) {
		app->notify(SAMPLE_SERVICE_ID,
			SAMPLE_INSTANCE_ID, SAMPLE_EVENT_ID, payload);
		usleep(1000 * 500);
	}
}

int main(int argc, char *argv[]) 
{
	app = vsomeip::runtime::get()->create_application("World");
	app->init();
	app->offer_service(SAMPLE_SERVICE_ID, SAMPLE_INSTANCE_ID);
	std::thread publisher(publish);
	app->start();
}
