#include <iomanip>
#include <iostream>
#include <sstream>

#include <condition_variable>
#include <thread>

#include <vsomeip/vsomeip.hpp>

#define SAMPLE_SERVICE_ID 0x1234
#define SAMPLE_INSTANCE_ID 0x5678
#define SAMPLE_EVENTGROUP_ID 0xdead
#define SAMPLE_EVENT_ID 0xbeef

std::shared_ptr<vsomeip::application> app;
std::mutex mutex;
std::condition_variable condition;

static void on_message(const std::shared_ptr<vsomeip::message> &_response)
{
	std::cout << "Event received!" << std::endl;
}

static void run(void) {
	std::unique_lock<std::mutex> its_lock(mutex);
	std::cout << "Wait availability" << std::endl;
	condition.wait(its_lock);
	std::cout << "Wait availability (done)" << std::endl;

	std::set<vsomeip::eventgroup_t> its_groups;
	its_groups.insert(SAMPLE_EVENTGROUP_ID);
	app->request_event(SAMPLE_SERVICE_ID, SAMPLE_INSTANCE_ID,
			SAMPLE_EVENT_ID, its_groups);
	app->register_message_handler(
		vsomeip::ANY_SERVICE, vsomeip::ANY_INSTANCE,
		vsomeip::ANY_METHOD, on_message);
					
	app->subscribe(SAMPLE_SERVICE_ID, SAMPLE_INSTANCE_ID,
			SAMPLE_EVENTGROUP_ID);
}

static void on_availability(vsomeip::service_t _service,
		vsomeip::instance_t _instance, bool _is_available)
{
	std::cout << "Service[" << std::setw(4) << std::setfill('0') <<
	std::hex << _service << "." << _instance << "] is " <<
	(_is_available ? "available." : "NOT available.") << std::endl;
	if (_is_available) {
		condition.notify_one();
	}
}

int main(int argc, char *argv[]) 
{
	app = vsomeip::runtime::get()->create_application("Hello");
	app->init();
	app->register_availability_handler(
		SAMPLE_SERVICE_ID, SAMPLE_INSTANCE_ID, on_availability);
	std::cout << "register_availability_handler is called" << std::endl;
	app->request_service(SAMPLE_SERVICE_ID, SAMPLE_INSTANCE_ID);
	std::thread subscriber(run);
	std::cout << "request_service is called" << std::endl;
	app->start();
}
