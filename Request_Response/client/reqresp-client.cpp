#include <iomanip>
#include <iostream>
#include <sstream>

#include <condition_variable>
#include <thread>

#include <vsomeip/vsomeip.hpp>

#define SAMPLE_SERVICE_ID 0x1234
#define SAMPLE_INSTANCE_ID 0x5678
#define SAMPLE_METHOD_ID 0x0421

std::shared_ptr<vsomeip::application> app;
std::mutex mutex;
std::condition_variable condition;

static void run(void) {
	std::unique_lock<std::mutex> its_lock(mutex);
	std::cout << "Wait availability" << std::endl;
	condition.wait(its_lock);
	std::cout << "Wait availability (done)" << std::endl;

	std::shared_ptr<vsomeip::message> request;
	request = vsomeip::runtime::get()->create_request();
	request->set_service(SAMPLE_SERVICE_ID);
	request->set_instance(SAMPLE_INSTANCE_ID);
	request->set_method(SAMPLE_METHOD_ID);

	std::shared_ptr<vsomeip::payload> its_payload;
       	its_payload = vsomeip::runtime::get()->create_payload();
	std::vector<vsomeip::byte_t> its_payload_data;
	for (vsomeip::byte_t i = 0; i < 10; i++) {
		its_payload_data.push_back(i % 256);
	}
	its_payload->set_data(its_payload_data);
	request->set_payload(its_payload);
	app->send(request);
}

static void on_message(const std::shared_ptr<vsomeip::message> &_response) {
	std::shared_ptr<vsomeip::payload> its_payload;
	vsomeip::length_t l;
       	its_payload = _response->get_payload();
	l = its_payload->get_length();

	std::stringstream ss;
	for (vsomeip::length_t i = 0; i < l; ++i) {
		ss << std::setw(2) << std::setfill('0') << std::hex <<
		static_cast<int>(*(its_payload->get_data() + i)) << " ";
	}

	std::cout << "CLIENT: Received message with Client/Session [" <<
		std::setw(4) << std::setfill('0') << std::hex <<
		_response->get_client() << "/" << std::setw(4) <<
		std::setfill('0') << std::hex << _response->get_session() <<
		"] " << ss.str() << std::endl;
}

static void on_availability(vsomeip::service_t _service,
		vsomeip::instance_t _instance, bool _is_available)
{
	std::cout << "Service[" << std::setw(4) << std::setfill('0') <<
	std::hex << _service << "." << _instance << "] is " <<
	(_is_available ? "available." : "NOT available.") << std::endl;
	if (_is_available) {
		condition.notify_one();
	}
}

int main(int argc, char *argv[]) 
{
	app = vsomeip::runtime::get()->create_application("Hello");
	app->init();
	app->register_availability_handler(
		SAMPLE_SERVICE_ID, SAMPLE_INSTANCE_ID, on_availability);
	std::cout << "register_availability_handler is called" << std::endl;
	app->request_service(SAMPLE_SERVICE_ID, SAMPLE_INSTANCE_ID);
	app->register_message_handler(SAMPLE_SERVICE_ID, SAMPLE_INSTANCE_ID,
			SAMPLE_METHOD_ID, on_message);
	std::thread sender(run);
	std::cout << "request_service is called" << std::endl;
	app->start();
}
